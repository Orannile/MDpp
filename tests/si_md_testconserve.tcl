# -*-shell-script-*-
# TCL script file to check energy conservation
# Si by SW, MEAM potential models
#
# To compile source code, e.g.
#   make sw build=R SYS=intel
#
# To run script, e.g.
#   bin/sw_intel tests/si_md_testconserve.tcl
#
# Typically we run for 2ps and examine energy fluctuation
#  during the second half of the simulation
#
# Typical result: (Velocity Verlet, time step 0.1fs, NP = 2744, T = 300K)
#  std(prop(100:200,6)) = 4.35e-5   max-min = 1.9e-4
# Typical result: (Gear6, time step 0.1fs, NP = 2744, T = 300K)
#  std(prop(100:200,6)) = 7.78e-9   max-min = 2.7e-8

source "scripts/Examples/Tcl/startup.tcl"

#*******************************************
# Definition of procedures
#*******************************************
proc initmd { n } { 
MD++ setnolog
MD++ setoverwrite
MD++ dirname = runs/si-md-$n
}
#end of proc initmd

proc create_perfect_crystal { } { MD++ {
#--------------------------------------------
# Create Perfect Lattice Configuration
#
latticestructure = diamond-cubic
latticeconst = 5.430949821 #(A) for Si
latticesize  = [ 1 0 0  7
                 0 1 0  7
                 0 0 1  7 ]
makecrystal finalcnfile = "perf.cn" writecn
} }

proc create_liquid { } { MD++ {
#--------------------------------------------
# Create Perfect Lattice Configuration
#
latticestructure = diamond-cubic
latticeconst = 5.430949821 #(A) for Si
latticesize  = [ 1 0 0  7
                 0 1 0  7
                 0 0 1  7 ]
makecrystal finalcnfile = "perf.cn" writecn
randomposition
} }

#-------------------------------------------------------------
proc openwindow { } { MD++ {
# Plot Configuration
#
atomradius = 0.67 bondradius = 0.3 bondlength = 0 #2.8285 #for Si
atomcolor = orange bondcolor = red backgroundcolor = gray70
plotfreq = 10  rotateangles = [ 0 0 0 1.25 ]
openwin  alloccolors rotate saverot eval plot
} }
#end of proc openwindow

#--------------------------------------------
proc exitmd { } { MD++ quit }
#end of proc exitmd

#--------------------------------------------
proc relax_fixbox { } { MD++ {
# Conjugate-Gradient relaxation
conj_ftol = 1e-6 conj_itmax = 1000 conj_fevalmax = 1000
conj_fixbox = 1  relax
} }
#end of proc relax_freebox

#--------------------------------------------
proc relax_freebox { } { MD++ {
# Conjugate-Gradient relaxation
conj_ftol = 1e-6 conj_itmax = 1000 conj_fevalmax = 1000
conj_fixbox = 0  relax
} }
#end of proc relax_freebox



#--------------------------------------------
proc setup_md { } { MD++ {     
equilsteps = 0  timestep = 0.0001 # (ps)
atommass = 28.0855 # (g/mol)
DOUBLE_T = 0
saveprop = 1 savepropfreq = 10 openpropfile #run
savecn = 1 savecnfreq = 10000 openintercnfile
plotfreq = 1000
vt2 = 2e28
#NHMass = [ 1.06e-2 1.29e-6 1.29e-6 1.29e-6 1.29e-6 1.29e-6 1.29e-6 ] #(for T=300K N = 2744)
NHMass = [ 1.06e-2 1.06e-2 1.06e-2 1.06e-2 1.06e-2 1.06e-2 1.06e-2 1.06e-2 ] #(for T=300K N = 2744)
#NHMass = [ 2.12e-2 2.58e-6 2.58e-6 2.58e-6 2.58e-6 2.58e-6 2.58e-6] #(for T=300K N = 2744)
#BNHMass = [ 1.06e-5 1.06e-5 1.06e-5 1.06e-5 1.06e-5 1.06e-5 1.06e-5 1.06e-5 ] #(for T=300K N = 2744)
#BNHMass = [ 1.06e-9 1.06e-9 1.06e-9 1.06e-9 1.06e-9 1.06e-9 1.06e-9 1.06e-9 ] #(for T=300K N = 2744)
wallmass = 2e3     # atommass * NP = 14380
boxdamp = 1e-2     # optimal damping for 216 atoms and wallmass 1e-3
saveH # Use current H as reference (H0), needed for specifying stress
fixboxvec  = [ 0 1 1 
               1 0 1
               1 1 0 ]
stress = [ 0 0 0
           0 0 0
           0 0 0 ]
output_fmt = "curstep EPOT KATOM Tinst EPBOX KBOX HELM HELMP TSTRESS_xx TSTRESS_yy TSTRESS_zz H_11 H_22 H_33" 
} }
#end of proc setup_md





#*******************************************
# Main program starts here
#*******************************************
# status 0:prepare liquid by MD (NPT) simulation
#        1:MC equilibration
#        2:compute free energy by MC switching simulation
#
# read in status from command line argument
if { $argc == 0 } {
 set status 0
} elseif { $argc > 0 } {
 set status [lindex $argv 0]
}
puts "status = $status"

if { $argc <= 1 } {
 set n 0
} elseif { $argc > 1 } {
 set n [lindex $argv 1]
}
puts "n = $n"
set n0 0



if { $status == 0 } {
        
 initmd $n

 create_perfect_crystal
 
 openwindow

 MD++ saveH # Use current H as reference (H0)

 setup_md
 MD++ ensemble_type = "NVE" integrator_type = "VVerlet"
 #MD++ ensemble_type = "NVT" integrator_type = "VVerlet" implementation_type = 2
 #MD++ ensemble_type = "NVE" integrator_type = "Gear6"
 
 MD++ srand48bytime  DOUBLE_T = 1  timestep = 0.001 # (ps)
 MD++ T_OBJ = 300 initvelocity 
 MD++ totalsteps = 2000
 #MD++ run

 #MD++ ensemble_type = "NVT" integrator_type = "VVerlet" implementation_type = 2 run
 #MD++ ensemble_type = "NVTC" integrator_type = "VVerlet" implementation_type = 1 NHChainLen = 4 run
 MD++ ensemble_type = "NVTC" integrator_type = "Gear6" NHChainLen = 4 run
 #MD++ ensemble_type = "NPT" integrator_type = "Gear6" NHChainLen = 4 run
 #MD++ ensemble_type = "NPTC" integrator_type = "Gear6" NHChainLen = 4 run
 
 MD++ finalcnfile = "crystal-equil.cn" writecn

 MD++_PrintVar EPBOX  "eV" "%20.12e"
 MD++_PrintVar KBOX  "eV" "%20.12e"
 
# MD++_PrintVar zetaBNHC  "eV*ps^2" "%20.12e"
# MD++_PrintVar zetaBNHCv "eV*ps^2" "%20.12e"
# MD++_PrintVar zetaBNHCa "eV*ps^2" "%20.12e"
# MD++_PrintVar zetaBNHC2 "eV*ps^2" "%20.12e"
# MD++_PrintVar zetaBNHC3 "eV*ps^2" "%20.12e"
# MD++_PrintVar zetaBNHC4 "eV*ps^2" "%20.12e" 
# MD++_PrintVar zetaBNHC5 "eV*ps^2" "%20.12e"
#
# MD++_PrintVar zetaBNHC(1)  "eV*ps^2" "%20.12e"
# MD++_PrintVar zetaBNHCv(1) "eV*ps^2" "%20.12e"
# MD++_PrintVar zetaBNHCa(1) "eV*ps^2" "%20.12e"
# MD++_PrintVar zetaBNHC2(1) "eV*ps^2" "%20.12e"
# MD++_PrintVar zetaBNHC3(1) "eV*ps^2" "%20.12e"
# MD++_PrintVar zetaBNHC4(1) "eV*ps^2" "%20.12e" 
# MD++_PrintVar zetaBNHC5(1) "eV*ps^2" "%20.12e"
 
 
 exitmd

} elseif { $status == 1 } {
        
 initmd $n

 #create_liquid
 MD++ incnfile = "../si-md-1/liquid-relax.cn" readcn
 
 openwindow

 #relax_fixbox
 #MD++ finalcnfile = "liquid-relax.cn" writecn
 
 MD++ saveH # Use current H as reference (H0)

 setup_md
 #MD++ ensemble_type = "NVE" integrator_type = "VVerlet"
 #MD++ ensemble_type = "NVT" integrator_type = "VVerlet" implementation_type = 2
 MD++ ensemble_type = "NVE" integrator_type = "Gear6"
 
 MD++ randseed = 12345 srand48  DOUBLE_T = 1  timestep = 0.001 # (ps)
 #MD++ srand48bytime  DOUBLE_T = 1  timestep = 0.001 # (ps)
 MD++ T_OBJ = 300 initvelocity 
 MD++ totalsteps = 2000

 #MD++ run

 #MD++ ensemble_type = "NVT" integrator_type = "VVerlet" implementation_type = 2 run
 #MD++ ensemble_type = "NVT" integrator_type = "Gear6" run
 #MD++ ensemble_type = "NVTC" integrator_type = "Gear6" NHChainLen = 4 run
 MD++ ensemble_type = "NPTC" integrator_type = "Gear6" NHChainLen = 4 run

 MD++_PrintVar zetaNHC  "eV*ps^2" "%20.12e"
 MD++_PrintVar zetaNHCv "eV*ps^2" "%20.12e"
 MD++_PrintVar zetaNHCa "eV*ps^2" "%20.12e"
 MD++_PrintVar zetaNHC2 "eV*ps^2" "%20.12e"
 MD++_PrintVar zetaNHC3 "eV*ps^2" "%20.12e"
 MD++_PrintVar zetaNHC4 "eV*ps^2" "%20.12e" 
 MD++_PrintVar zetaNHC5 "eV*ps^2" "%20.12e"

 MD++_PrintVar zetaNHC(1)  "eV*ps^2" "%20.12e"
 MD++_PrintVar zetaNHCv(1) "eV*ps^2" "%20.12e"
 MD++_PrintVar zetaNHCa(1) "eV*ps^2" "%20.12e"
 MD++_PrintVar zetaNHC2(1) "eV*ps^2" "%20.12e"
 MD++_PrintVar zetaNHC3(1) "eV*ps^2" "%20.12e"
 MD++_PrintVar zetaNHC4(1) "eV*ps^2" "%20.12e" 
 MD++_PrintVar zetaNHC5(1) "eV*ps^2" "%20.12e"

#
# MD++ ensemble_type = "NVT" integrator_type = "Gear6" run
#
# MD++_PrintVar zeta  "eV*ps^2" "%20.12e"
# MD++_PrintVar zetav "eV*ps^2" "%20.12e"
# MD++_PrintVar zetaa "eV*ps^2" "%20.12e"
# MD++_PrintVar zeta2 "eV*ps^2" "%20.12e"
# MD++_PrintVar zeta3 "eV*ps^2" "%20.12e"
# MD++_PrintVar zeta4 "eV*ps^2" "%20.12e" 
# MD++_PrintVar zeta5 "eV*ps^2" "%20.12e"

# 
 MD++ finalcnfile = "liquid-equil.cn" writecn
 exitmd

} else {
        
 puts "unknown status = $status"
 exitmd 

} 
#-------------------------------------------
