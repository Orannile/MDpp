print("Hello! ")

#*******************************************
# Definition of functions
#*******************************************

mdpp.init <- function(name) {
    if (!is.character(name))
	stop("argument x must be character")
    out <- .C("md_init", as.character(name))
    return (out)
}

mdpp.cmd <- function(name) {
    if (!is.character(name))
	stop("argument x must be character")
    out <- .C("md_cmd", as.character(name))
    return (out)
}

mdpp.get <- function(name) {
    if (!is.character(name))
	stop("argument x must be character")
    out <- .C("md_get", as.character(name))
    return (out)
}

mdpp.get_array <- function(name,id) {
    if (!is.character(name))
	stop("argument x must be character")
    out <- .C("md_get_array", as.character(name), as.integer(length(id)), as.integer(id))
    return (out)
}

mdpp.set_array <- function(name,id,val) {
    if (!is.character(name))
	stop("argument x must be character")
    out <- .C("md_set_array", as.character(name), as.integer(length(id)), as.integer(id), as.double(val))
    return (out)
}

foo <- function(x) {
    if (!is.numeric(x))
        stop("argument x must be numeric")
    out <- .C("foo", n=as.integer(length(x)), x=as.double(x))
    return (out$x)
}
