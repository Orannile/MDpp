import sys
sys.path.insert(0, "./bin")

import mdpp

# Run MD++ script

mdpp.cmd('''
# -*-shell-script-*-
# MD code of L-J Argon
setnolog
setoverwrite
dirname = runs/ar-example
#------------------------------------------------------------
#Read in structure
#
#incnfile = relaxed.cn readcn
#
#------------------------------------------------------------
#Create Perfect Lattice Configuration
#
latticestructure = face-centered-cubic NNM = 100 element0 = Ar
latticeconst = 5.314370 #(A) for Ar
#latticesize = [ 1  1 -2 4    #(x)
#                1  1  1 10   #(y)
#                1 -1  0 4  ] #(z)
latticesize = [ 1 0 0  4
                0 1 0  4
                0 0 1  4  ]
makecrystal finalcnfile = perf.cn writecn
#finalcnfile = "lj256.coords_in" writePINYMD
#input = [ 1 0 ] fixatoms_by_ID removefixedatoms
#------------------------------------------------------------
#
#Plot Configuration
#
atomradius = 0.67 bondradius = 0.3 bondlength = 0 
atomcolor = cyan highlightcolor = purple  bondcolor = red
backgroundcolor = gray fixatomcolor = yellow
color00 = "orange"  color01 = "red"    color02 = "green"
color03 = "magenta" color04 = "cyan"   color05 = "purple"
color06 = "gray80"  color07 = "white"
plot_color_windows = [ 2
                          -10 -0.064 0  #color00 = orange
                          -0.064 10  5  #color05 = purple
                     ]
plot_atom_info = 3 
plotfreq = 10
rotateangles = [ 0 0 0 1.2 ]
#rotateangles = [ 0 0 0 1.6 ]
win_width = 500 win_height = 500
openwin alloccolors rotate saverot refreshnnlist eval plot
#sleep quit
#------------------------------------------------------------
#Conjugate gradient relaxation
#
conj_ftol = 1e-7 conj_itmax = 1000 conj_fevalmax = 10000
conj_fixbox = 1 #conj_monitor = 1 conj_summary = 1
#relax finalcnfile = relaxed.cn writecn
#eval sleep quit
''')

import time
sleep_seconds = 10
print("Python is going to sleep for %d seconds."%sleep_seconds)
time.sleep(sleep_seconds)

