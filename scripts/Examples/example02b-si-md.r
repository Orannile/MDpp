#!/usr/bin/env Rscript
# MD code of Stinger-Weber Silicon
# Run as: 
#   Rscript scripts/Examples/example02b-si-md.r

# Load MD++ library
dyn.load("bin/sw_r.so")

source("scripts/Examples/R/startup.r")

mdpp.init("si-md")

initmd <- function(T) {
    mdpp.cmd("setnolog setoverwrite")
    mdpp.cmd(sprintf("dirname = runs/si-example-%.1f",T))
}

makecrystal <- function(nx,ny,nz) {
    # create perfect lattice configuration
    mdpp.cmd('crystalstructure = diamond-cubic')
    mdpp.cmd('latticeconst = 5.4309529817532409') # (A) for Si
    mdpp.cmd('element0 = Silicon')
    mdpp.cmd(sprintf('latticesize = [ 1 0 0 %d	0 1 0 %d  0 0 1 %d ]',nx,ny,nz))
    mdpp.cmd('makecrystal  finalcnfile = perf.cn writecn')
}

setup_window <- function() {
    # plot settings
    mdpp.cmd("
     atomradius = 0.67 bondradius = 0.3 bondlength = 2.8285 #for Si
     atomcolor = orange highlightcolor = purple
     bondcolor = red backgroundcolor = gray70
    #plot_color_bar = [ 1 -4.85 -4.50 ]  highlightcolor = red
     plotfreq = 10  rotateangles = [ 0 0 0 1.25 ]
    ")
}

openwindow <- function() {
    #Configure and open a plot
    setup_window()

    #### setup_window
    mdpp.cmd('openwin  alloccolors rotate saverot eval plot')
}

relax_fixbox <- function() {
    #Conjugate-Gradient relaxation
    mdpp.cmd("
    conj_ftol = 1e-7 conj_itmax = 1000 conj_fevalmax = 10000
    conj_fixbox = 1 #conj_monitor = 1 conj_summary = 1
    relax
    ")
}

relax_freebox <- function() {
    #Conjugate-Gradient relaxation
    mdpp.cmd("
    conj_ftol = 1e-7 conj_itmax = 1000 conj_fevalmax = 10000
    conj_fixbox =  #conj_monitor = 1 conj_summary = 1
    relax
    ")
}

setup_md <- function() {
    # MD settings (without running MD)
    mdpp.cmd("
    equilsteps = 0  totalsteps = 100 timestep = 0.0001 # (ps)
    atommass = 28.0855 # (g/mol)
   #atomTcpl = 200.0 boxTcpl = 20.0
    DOUBLE_T = 0
    srand48bytime
    initvelocity totalsteps = 10000 saveprop = 0
    saveprop = 1 savepropfreq = 10 openpropfile
    savecn = 1	savecnfreq = 100
    writeall = 1
    savecfg = 1 savecfgfreq = 100
    ensemble_type = \"NVT\" integrator_type = \"VVerlet\"
    implementation_type = 0 vt2=1e28

    totalsteps = 20000
    output_fmt = \"curstep EPOT KATOM Tinst HELM HELMP TSTRESS_xx TSTRESS_yy TSTRESS_zz\"
    plotfreq = 50 #autowritegiffreq = 10
    ")
}

# Main program

args = commandArgs(trailingOnly=TRUE)
if (length(args)==0) {
  status = 0
} else {
  status = as.numeric(args[1])
}

if (length(args)<=1) {
  T_OBJ = 300
} else {
  T_OBJ = as.numeric(args[2])
}

if (status == 0) {
    #create crystal, relax, run MD/NVT simulation

    initmd(T_OBJ)

    makecrystal(4,4,4)

    openwindow()

    relax_fixbox()
    mdpp.cmd('finalcnfile = relaxed.cn writecn')
    mdpp.cmd('finalcnfile = relaxed.lammps writeLAMMPS')
    mdpp.cmd('finalcnfile = relaxed.cfg writeatomeyecfg')
    setup_md()
    mdpp.cmd(sprintf('T_OBJ = %.1f',T_OBJ))
    mdpp.cmd('initvelocity')
    mdpp.cmd('run')

    mdpp.cmd('finalcnfile = si100.cn writecn')
    mdpp.cmd('finalcnfile = si100.cfg writeatomeyecfg')

    mdpp.cmd('sleep quit')

} else if (status == 1) {
    #read in relaxed, NVT

    initmd(T_OBJ)

    mdpp.cmd('incnfile = si100.cn readcn')

    openwindow()

    # Testing
    mdpp.get("totalsteps")
    mdpp.get_array("SR",c(1,2,3))
    mdpp.set_array("SR",c(1,2,3),c(0.1,0.2,0.3))

    mdpp.cmd('sleep quit')

} else {
    print(sprintf('unknown argument. %d',status))

    mdpp.cmd('quit')

}
