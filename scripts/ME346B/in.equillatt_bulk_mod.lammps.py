# load lammps module
import os, sys
lammpsdir = os.environ['LAMMPS_DIR']
sys.path.append(os.path.join(lammpsdir, 'python'))
from lammps import lammps

structure_list = ['diamond', 'fcc', 'bcc', 'sc']
lat_const_list = [5.4309, 4.1465, 3.245, 2.612]

#*******************************************
# Definition of procedures
#*******************************************

def initmd():
    # set up runs directory
    runsdir = os.path.join(lammpsdir, 'runs/si_bulkmod')
    os.makedirs(runsdir, exist_ok=True)
    os.chdir(runsdir)

    # set lammps simulation object
    lmp = lammps()
    lmp.command('log log.Si_test')        # log file
    
    return lmp

def makecrystal(lmp, st, a):
    '''Create Perfect Lattice Configuration'''
    lmp.command('clear')
    lmp.command('units metal')            # unit definition (https://lammps.sandia.gov/doc/units.html)
    lmp.command('atom_style atomic')
    lmp.command('boundary p p p')
    lmp.command('lattice ' + st + (' %f orient x 1 0 0 orient y 0 1 0 orient z 0 0 1'%a))
    lmp.command('region  box block 0 4 0 4 0 4 units lattice')
    lmp.command('create_box 1 box')             # create simulation box with 1 componenet
    lmp.command('create_atoms 1 region box')    # fill the box with atom type 1
    lmp.command('mass 1 28.085')                # set the atomic mass of Silicon
    # set potential: Stillinger and Weber, PRB 31, 5262 (1985)
    lmp.command('pair_style sw')
    lmp.command('pair_coeff * * ../../potentials/Si.sw Si')
    return lmp

def readnwrite(lmp, fout, st, a):
    '''read and write properties'''
    heading = r'#    lattice constant     ' + \
               r'  number density       ' + \
               r'  atomic volume        ' + \
               r'  potential energy     ' + \
               r'  lattice energy'
    print(heading, file=fout)
    print(heading)
    for i in range(-50, 51):
        latt = a + i/10000.0
        lmp = makecrystal(lmp, st, a)
        lmp.command('minimize 1e-3 1e-4 1000 1000')
        Epot = lmp.get_thermo('pe')
        N = lmp.get_thermo('atoms')
        vol = lmp.get_thermo('vol')
        Elat = Epot / N
        rho = N / vol
        AtomVol = vol / N
        print(r'%23.12e%23.12e%23.12e%23.12e%23.12e'%(latt, rho, AtomVol, Epot, Elat), file=fout)
        print(r'%23.12e%23.12e%23.12e%23.12e%23.12e'%(latt, rho, AtomVol, Epot, Elat))
    return lmp
        
#*******************************************
# Main script starts here
#*******************************************
lmp = initmd()

for i in range(len(structure_list)):
    st = structure_list[i]
    a  = lat_const_list[i]
    with open(st + '.dat', 'w') as fout:
        readnwrite(lmp, fout, st, a)
