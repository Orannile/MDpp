# MD simulations of an interstitial in a crystal of BCC Ta

import sys, os
mdpp_dir = os.environ['MDPLUS_DIR']
print('MD++ root directory:', mdpp_dir)
sys.path.insert(0, os.path.join(mdpp_dir, 'bin'))

import mdpp
import numpy as np

#********************************************
# Definition of functions
#********************************************

#--------------------------------------------
# Initialization
def init_ising(h, kBT):
    #mdpp.cmd("setnolog")
    mdpp.cmd("setoverwrite")
    mdpp.cmd("dirname = runs/ising-2d_h%g_kBT%g"%(h,kBT))

    # initialize random number generator by time
    mdpp.cmd("srand48bytime srandbytime")

    # initial configuration
    mdpp.cmd("NX = 100  NY = 100 input = -1  initspin ")
    # physical parameters
    mdpp.cmd("J = 1")          # interaction strength
    mdpp.cmd("H = %g"%(h))     # applied field
    mdpp.cmd("kBT = %g"%(kBT)) # temperature
    mdpp.cmd("zipfiles = 1")

#--------------------------------------------
# Initialization of umbrella sampling simulation
def init_ising_umb(h, kBT, w_id):
    #mdpp.cmd("setnolog")
    mdpp.cmd("setoverwrite")
    mdpp.cmd("dirname = runs/ising-2d_h%g_kBT%g/UMB_%d"%(h,kBT,w_id))

    # initialize random number generator by time
    mdpp.cmd("srand48bytime srandbytime")

    # initial configuration
    mdpp.cmd("NX = 100  NY = 100 input = -1  initspin ")
    # physical parameters
    mdpp.cmd("J = 1")                   # interaction strength
    mdpp.cmd("H = %g"%(h))              # applied field
    mdpp.cmd("kBT = %g"%(kBT))          # temperature
    mdpp.cmd("zipfiles = 1")

#--------------------------------------------
# Plot setting
def setup_window():
    mdpp.cmd('''
    atomradius = 1.0 win_width = 480 win_height = 480 rotateangles = [ 0 0 0 1.7]
    color00 = white color01 = blue backgroundcolor = gray80
    ''')

def openwindow():
    setup_window()
    mdpp.cmd("openwin alloccolors rotate saverot plot")

#--------------------------------------------
# MC simulation setting
def setup_mc():
    mdpp.cmd('printfreq = 100   savecn = 0 ')
    mdpp.cmd('saveprop = 1 savepropfreq = 1 plotfreq = 100  openpropfile ')
    mdpp.cmd('calcryfreq = 1 calcryfreq1 = 250 allocDFS ') # Settings for identifying largest cluster

#*******************************************
# Main program starts here
#*******************************************
# status 0: MC simulation at given field strength and temperature
#        1: Umbrella sampling simulation
#
#--------------------------------------------
# Get command line argument
status = int(sys.argv[1]) if len(sys.argv) > 1 else 0
print('status = %d'%(status))

h = float(sys.argv[2]) if len(sys.argv) > 2 else 0
print('h = %g'%(h))

kBT = float(sys.argv[3]) if len(sys.argv) > 3 else 1.7
print('kBT = %g'%(kBT))

totsteps = int(sys.argv[4]) if len(sys.argv) > 4 else 10000
print('totsteps = %d'%(totsteps))

if status != 0:
    w_id = int(sys.argv[5]) if len(sys.argv) > 5 else 0
    print('w_id = %d'%(w_id))

if status == 0:
    # Monte Carlo simulation
    print("staus == 0: MC simulations")
    init_ising(h, kBT)

    setup_window()
    # Comment out the following line if you don't want to open a window
    openwindow()

    setup_mc()

    mdpp.cmd("totalsteps = %d"%(totsteps))
    mdpp.cmd("MCrun")

    data = np.loadtxt("prop.out")
    Ndata = data.shape[0]
    Stot = data[Ndata//5:,1] # total spin during simulation
    Nc   = data[Ndata//5:,4] # size of largest cluster during simulation
    Nspin = 100*100          # size of the Ising model
    M_avg = np.mean(Stot)/Nspin
    M_std = np.std (Stot)/Nspin

    analytic_M = lambda t: (1.0 - 1.0 / np.sinh(2.0/t)**4)**(0.125)
    M_anl = analytic_M(kBT)

    print(" ")
    print("Magnetization from Monte Carlo Simulation = %5f ± %5f"%(M_avg, M_std))
    print("Magnetization from Analytic Onsager Solution = %5f"%(M_anl))
    print("Average Size of Largest Cluster = %5f"%(np.mean(Nc)))
    print(" ")

elif status == 1:
    # Umbrella sampling simulation
    print("staus == 1: Umbrella Sampling simulations")
    init_ising_umb(h, kBT, w_id)

    setup_window()
    # Comment out the following line if you don't want to open a window
    openwindow()

    setup_mc()

    # umbrella sampling parameters
    n_c = 12.0 * w_id
    print('n_c = %g'%(n_c))

    mdpp.cmd("YES_UMB = 1  n_center = %g  delta_n = %g"%(n_c, 8))
    mdpp.cmd("UMB_K = %g"%(0.16 * kBT)) # stiffness of bias potential
    mdpp.cmd("totalsteps = %d"%(totsteps))
    mdpp.cmd("MCrun")

    data = np.loadtxt("prop.out")
    Ndata = data.shape[0]
    Nc   = data[Ndata//5:,4] # size of largest cluster during simulation

    print(" ")
    print("Average Size of Largest Cluster = %5f"%(np.mean(Nc)))
    print(" ")

else:
    print("unknown status = %d"%(status))
 
#---------------------------------------------
# Sleep for a couple of seconds
import time
if not 'sleep_seconds' in locals():
    sleep_seconds = 2
print("Python is going to sleep for %d seconds."%sleep_seconds)
time.sleep(sleep_seconds)
