import sys, os
mdpp_dir = os.environ['MDPLUS_DIR']
print('MD++ root directory:', mdpp_dir)
sys.path.insert(0, os.path.join(mdpp_dir, 'bin'))

import mdpp

#--------------------------------------------
# make perfect crystal of Molybdenum
mdpp.cmd('''
setnolog
setoverwrite
dirname = runs/mo-vac # specify run directory
''')

#--------------------------------------------
# Read the potential file
mdpp.cmd('potfile = ' + os.path.join(mdpp_dir, 'potentials', 'mo_pot'))
mdpp.cmd('readpot')

#--------------------------------------------
#Create Perfect Lattice Configuration

mdpp.cmd('''
crystalstructure = body-centered-cubic latticeconst = 3.1472 #(A)
latticesize = [ 1 0 0 5
                0 1 0 5
                0 0 1 5 ]
makecrystal finalcnfile = perf.cn writecn
eval # evaluate the potential of perfect crystal
''')

#--------------------------------------------
# Create Vacancy
mdpp.cmd('''
input = [ 1      # number of atoms to be fixed
          0]     # index of an atom to be fixed
fixatoms_by_ID   # fix a set of atoms by their indices
removefixedatoms # remove fixed atoms
finalcnfile = movac.cn writecn
eval # evaluate the vacancy-formed crystal
''')

#---------------------------------------------
# Plot Configuration
mdpp.cmd('''
atomradius = 1.0 bondradius = 0.3 bondlength = 0
atomcolor = blue highlightcolor = purple backgroundcolor = gray
bondcolor = red   fixatomcolor = yellow
plotfreq = 10 win_width = 600    win_height = 600
plot_atom_info = 3
color00 = "orange" color01 = "purple" color02 = "green"
color03 = "magenta" color04 = "cyan"   color05 = "purple"
color06 = "gray80" color07 = "white"
plot_color_windows = [ 2             # number of color windows
                      -10  -6.8   6  # color06 = gray80
                      -6.7 -6.0   0  # color00 = orange
                     ]
rotateangles = [ 0 0 0 1 ]
openwin alloccolors rotate saverot plot
''')

#---------------------------------------------
# Sleep for a couple of seconds
import time
sleep_seconds = 2
print("Python is going to sleep for %d seconds."%sleep_seconds)
time.sleep(sleep_seconds)
