import sys, os
mdpp_dir = os.environ['MDPLUS_DIR']
print('MD++ root directory:', mdpp_dir)
sys.path.insert(0, os.path.join(mdpp_dir, 'bin'))

import mdpp


mdpp.cmd('''
# -*-shell-script-*-
# make perfect crystal of copper
#
# setnolog
setoverwrite
dirname = runs/cu_supercell # specify run directory
''')

# mdpp.cmd('potfile = ' + os.path.join(mdpp_dir, 'potentials/EAMDATA', 'eamdata.Cu'))
# mdpp.cmd('eamgrid = 5000 readeam')

mdpp.cmd('''
#--------------------------------------------
#Create Perfect Lattice Configuration
#
crystalstructure = face-centered-cubic latticeconst = 3.58 #(A) for Cu
latticesize = [  0  1 -1  5
                -2  1  1  4
                 1  1  1  4 ]
makecrystal 
NNM = 60 NIC = 200
''')

mdpp.cmd('finalcnfile = cu_supercell.cn writecn')
mdpp.cmd('finalcnfile = cu_supercell.lammps writeLAMMPS')

mdpp.cmd('''
#-------------------------------------------------------------
#Plot Configuration
atomradius = 1.0 bondradius = 0.3 bondlength = 0
atomcolor = blue highlightcolor = purple backgroundcolor = black 
bondcolor = red fixatomcolor = yellow
rotateangles = [ 0 0 0 1.0 ]
#
win_width = 600 win_height = 600
openwin alloccolors rotate saverot refreshnnlist plot
''')

import time
sleep_seconds = 10
print("Python is going to sleep for %d seconds."%sleep_seconds)
time.sleep(sleep_seconds)

