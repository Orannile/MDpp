import sys, os
mdpp_dir = os.environ['MDPLUS_DIR']
print('MD++ root directory:', mdpp_dir)
sys.path.insert(0, os.path.join(mdpp_dir, 'bin'))

import mdpp

mdpp.cmd('''
# -*-shell-script-*-
# make perfect crystal of Argon
#
 setnolog
setoverwrite
dirname = runs/ar-large-example # specify run directory
''')


mdpp.cmd('''
#--------------------------------------------
#Create Perfect Lattice Configuration
#
crystalstructure = face-centered-cubic latticeconst = 5.260 #(A) for Ar
NNM = 120 
''')

fo = open("ar_eng.dat","w")

mdpp.cmd('''
latticesize = [  1   0  0  4
                 0   1  0  4
                 0   0  1  4]
makecrystal  finalcnfile = perf-4x4x4.cn writecn
''')

mdpp.cmd(" eval ")
N = mdpp.get("NP")
E = mdpp.get("EPOT")
print("4 %5d    %.12f  %.12f"%(N,E,E/N), file=fo)


mdpp.cmd('''
latticesize = [  1   0  0  5
                 0   1  0  5
                 0   0  1  5]
makecrystal  finalcnfile = perf-5x5x5.cn writecn
''')

mdpp.cmd(" eval ")
N = mdpp.get("NP")
E = mdpp.get("EPOT")
print("5 %5d    %.12f  %.12f"%(N,E,E/N), file=fo)


mdpp.cmd('''
latticesize = [  1   0  0  6
                 0   1  0  6
                 0   0  1  6]
makecrystal  finalcnfile = perf-6x6x6.cn writecn
''')


mdpp.cmd(" eval ")
N = mdpp.get("NP")
E = mdpp.get("EPOT")
print("6 %5d    %.12f  %.12f"%(N,E,E/N), file=fo)

mdpp.cmd('''
latticesize = [  1   0  0  7
                 0   1  0  7
                 0   0  1  7]
makecrystal  finalcnfile = perf-7x7x7.cn writecn
''')


mdpp.cmd(" eval ")
N = mdpp.get("NP")
E = mdpp.get("EPOT")
print("7 %5d    %.12f  %.12f"%(N,E,E/N), file=fo)


fo.close()

