{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "toc": true
   },
   "source": [
    "<h1>Table of Contents<span class=\"tocSkip\"></span></h1>\n",
    "<div class=\"toc\"><ul class=\"toc-item\"><li><span><a href=\"#Tutorial-04:-Neighbor-List-and-Periodic-Boundary-Condition\" data-toc-modified-id=\"Tutorial-04:-Neighbor-List-and-Periodic-Boundary-Condition-4\"><span class=\"toc-item-num\">4&nbsp;&nbsp;</span>Tutorial 04: Neighbor List and Periodic Boundary Condition</a></span><ul class=\"toc-item\"><li><span><a href=\"#Initialization\" data-toc-modified-id=\"Initialization-4.1\"><span class=\"toc-item-num\">4.1&nbsp;&nbsp;</span>Initialization</a></span></li><li><span><a href=\"#Lennard-Jones-Potential\" data-toc-modified-id=\"Lennard-Jones-Potential-4.2\"><span class=\"toc-item-num\">4.2&nbsp;&nbsp;</span>Lennard-Jones Potential</a></span></li><li><span><a href=\"#Cell-List-and-Neighbor(Verlet)-List\" data-toc-modified-id=\"Cell-List-and-Neighbor(Verlet)-List-4.3\"><span class=\"toc-item-num\">4.3&nbsp;&nbsp;</span>Cell List and Neighbor(Verlet) List</a></span><ul class=\"toc-item\"><li><span><a href=\"#Cell-list\" data-toc-modified-id=\"Cell-list-4.3.1\"><span class=\"toc-item-num\">4.3.1&nbsp;&nbsp;</span>Cell list</a></span></li><li><span><a href=\"#Periodic-Boundary-Condition\" data-toc-modified-id=\"Periodic-Boundary-Condition-4.3.2\"><span class=\"toc-item-num\">4.3.2&nbsp;&nbsp;</span>Periodic Boundary Condition</a></span></li><li><span><a href=\"#Neighbor(Verlet)-List\" data-toc-modified-id=\"Neighbor(Verlet)-List-4.3.3\"><span class=\"toc-item-num\">4.3.3&nbsp;&nbsp;</span>Neighbor(Verlet) List</a></span></li></ul></li></ul></li></ul></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Tutorial 04: Neighbor List and Periodic Boundary Condition\n",
    "Yifan Wang and Wei Cai\n",
    "\n",
    "**2019-04-06**\n",
    "\n",
    "In this tutorial, we will implement neighbor list and periodic boundary condition, and improve the efficiency of Lennard-Jones potential energy evaluation.\n",
    "\n",
    "## Initialization\n",
    "\n",
    "\n",
    "**1. This notebook uses the following extensions, please set them up in nbextensions before using this notebook**\n",
    "* Table of Content (2)\n",
    "\n",
    "<sub>Instructions for nbextension installation is in [Tutorial 01 1.2.2.2](Tutorial%2001%20-%20Introduction%20to%20MD%2B%2B.ipynb)</sub>\n",
    "\n",
    "**2. If you have not, please add the following 3 lines into `~/.bashrc`, and reboot Ubuntu to setup the environment variables**\n",
    "\n",
    "These environmental variables specifies the MD++ root directory, the MD++ compiling system, and name of the MD++ executable, respectively."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "    export MDPLUS_DIR=$HOME/Codes/MD++.git\n",
    "    export MDPLUS_EXE=python3\n",
    "    export MDPLUS_SYS=gpp"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**3. Check if environmental variables are set. Change current working directory into the MD++ root folder**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "\n",
    "envvar_test = True\n",
    "envvars = ['MDPLUS_DIR', 'MDPLUS_EXE', 'MDPLUS_SYS']\n",
    "for envvar in envvars:\n",
    "    if envvar not in os.environ.keys():\n",
    "        print('Environment variable \"'+envvar+'\" not set')\n",
    "        envvar_test = False\n",
    "    else:\n",
    "        print('Environment variable \"'+envvar+'\" set to '+os.environ[envvar])\n",
    "\n",
    "if not envvar_test:\n",
    "    raise OSError\n",
    "\n",
    "mdpp_dir = os.environ['MDPLUS_DIR']\n",
    "os.chdir(mdpp_dir)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Lennard-Jones Potential\n",
    "\n",
    "The interatomic interaction potential between atoms is represented as a function of all atoms' coordinates:\n",
    "\n",
    "$$\n",
    "V(\\{\\vec{r}_i\\})\\equiv V(\\vec{r}_1, \\vec{r}_2, \\dots, \\vec{r}_N)\n",
    "$$\n",
    "\n",
    "The interaction potential is usually represented as functions of vectors measuring the relative position between atoms.  For example, $\\vec{r}_{ij} = \\vec{r}_j - \\vec{r}_i$ is the vector pointing from atom $i$ to atom $j$.  A simple interatomic potential model is the _pair potential_ (which assumes that the potential energy is the superposition of pair-wise contributions).\n",
    "\n",
    "$$\n",
    "V(\\{\\vec{r}_i\\}) = \\sum_{i=1}^N\\sum_{j=i+1}^{N}\\phi(|\\vec{r}_{ij}|)\n",
    "$$\n",
    "\n",
    "where $\\phi(r)$ is a function of the inter-atomic distance $r$.  A common pair potential is the Lennard-Jones potential:\n",
    "\n",
    "$$\n",
    "\\phi(r)=4\\epsilon_0\\left[\\left({r\\over \\sigma_0}\\right)^{-12}-\\left({r\\over \\sigma_0}\\right)^{-6}\\right]\n",
    "$$\n",
    "\n",
    "with only two parameters $\\epsilon_0$ and $\\sigma_0$.  Even for more complicated interatomic potential models with many-body effects, the pair potential is commonly included as a contribution to the potential energy. \n",
    "\n",
    "The force contribution from the pair-wise interaction between atoms $i$ and $j$ is:\n",
    "\n",
    "$$\n",
    "\\vec{f}(\\vec{r})=-\\frac{\\partial \\phi(r)}{\\partial \\vec{r}}=\n",
    "-4\\epsilon_0\\left[-12\\left({r\\over\\sigma_0}\\right)^{-13} + 6\\left({r\\over\\sigma_0}\\right)^{-7}\\right]\n",
    "\\left(\\frac{\\vec{r}}{r\\sigma_0}\\right)\n",
    "$$\n",
    "\n",
    "This force is to be added to atom $j$ and the opposite force should be added to atom $i$.\n",
    "\n",
    "The following function defines the (untruncated) Lennard-Jones potential for plotting purposes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "def ljval_original(drij, sigma0, epsilon0):\n",
    "    '''LJ potential for atom pair i, j\n",
    "    \n",
    "        Parameters\n",
    "        ----------\n",
    "        drij : float, dimension (npairs, 3)\n",
    "            distance vectors (Angstrom)\n",
    "        sigma0, epsilon0 : float\n",
    "            parameters for Lennard-Jones potential\n",
    "\n",
    "        Returns\n",
    "        -------\n",
    "        phi : float, dimension (npairs, )\n",
    "            potential energies of each pair (eV)\n",
    "        f : float, dimension (npairs, 3)\n",
    "            force vector between each pair of atoms (eV/A)\n",
    "            \n",
    "    '''\n",
    "    r = np.linalg.norm(drij, axis=-1, keepdims=True)\n",
    "    rt = r/sigma0\n",
    "    phi = 4*epsilon0*(rt**(-12)-rt**(-6))\n",
    "    f = -4*epsilon0*( -12*rt**(-13) + 6*rt**(-7) )* drij /r/sigma0\n",
    "    return phi, f"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The LJ potential parameters for Argon are $\\sigma_0=3.405\\,\\mathring{A}$, $\\epsilon_0=119.8\\,k_B$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# LJ parameters for Argon\n",
    "kB = 1.380662e-23/1.6021892e-19  # eV/K\n",
    "epsilon0 = 119.8*kB   # eV\n",
    "sigma0   = 3.405      # Angstrom\n",
    "\n",
    "rmax = 14\n",
    "r = np.linspace(3, rmax, 500)\n",
    "phi, f = ljval_original(r[:, np.newaxis], sigma0, epsilon0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We now visualize the energy and force of the LJ potential as a function of interatomic distance $r$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# plotting the LJ potential function\n",
    "plt.figure()\n",
    "plt.plot(r, phi, label=r'$\\phi$ (eV)')\n",
    "plt.plot(r, f, label=r'$|\\vec{f}|$ (eV/A)')\n",
    "plt.plot([0, rmax], [-epsilon0, -epsilon0], '--')\n",
    "plt.xticks(list(range(0, rmax+2, 2)) + [sigma0, ], \n",
    "           list(range(0, rmax+2, 2)) + [r'$\\sigma_0$',], fontsize=12)\n",
    "plt.yticks([-epsilon0, 0], [r'$-\\epsilon_0$', 0], fontsize=12)\n",
    "plt.plot([0, rmax], [0, 0], '--k')\n",
    "plt.xlim([0, rmax])\n",
    "plt.ylim([-1.5*epsilon0, 3*epsilon0])\n",
    "plt.xlabel(r'$r(\\AA)$', fontsize=16)\n",
    "plt.legend(fontsize=16)\n",
    "plt.title('Lennard-Jones Potential', fontsize=20)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**<font size=+1>Exercise 1</font>**\n",
    "\n",
    "From the above figure, the potential $\\phi\\rightarrow0$ when $r\\rightarrow\\infty$, i.e. the potential energy never goes to exactly zero no matter how large is $r$.  To save computational time, we can set the potential energy to be exactly zero when $r$ exceeds a certain cut-off distance $r_c$.  However, simply truncating the $\\phi(r)$ function will lead to a discontinuity at $r = r_c$, which creates problems for energy minization (MS) and time integration (MD) algorithms. To avoid artifacts in atomistic simulations, it is best to make sure that both the energy and its first derivative (i.e. force) vanishes at $r = r_c$, i.e. $\\phi(r_c)=\\phi'(r_c)=0$. Specifically, for the LJ potential this can be done by adding a linear term to $\\phi(r)$ when $r < r_c$, i.e.,\n",
    "\n",
    "$$\n",
    "\\phi(r) = \\begin{cases}\n",
    "4\\epsilon_0\\left[\\left({r/\\sigma_0}\\right)^{-12}-\\left({r/\\sigma_0}\\right)^{-6}\\right]+A+Br,\\quad r<r_c\\\\\n",
    "0,\\quad r\\geq r_c\\end{cases}\n",
    "$$\n",
    "\n",
    "Find values for $A$ and $B$ such that the truncated LJ potential and its first derivative with respect to r become zero at $r = r_c$.\n",
    "\n",
    "**(a) Implement `ljval` function for the smoothly truncated LJ potential**\n",
    "\n",
    "You need to find the correct expression for constants $A$ and $B$ and insert them in the cell below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def ljval(drij, rc, sigma0, epsilon0):\n",
    "    '''LJ potential for atom pair i, j\n",
    "    \n",
    "        Parameters\n",
    "        ----------\n",
    "        drij : float, dimension (npairs, 3)\n",
    "            distance vectors (Angstrom)\n",
    "        rc : float\n",
    "            cut-off radius (Angstrom)\n",
    "        sigma0, epsilon0 : float\n",
    "            parameters for Lennard-Jones potential (Angstrom, eV)\n",
    "\n",
    "        Returns\n",
    "        -------\n",
    "        phi : float, dimension (npairs, )\n",
    "            potential energies of each pair (eV)\n",
    "        f : float, dimension (npairs, 3)\n",
    "            force vector between each pair of atoms (eV/A)\n",
    "            \n",
    "    '''\n",
    "    r = np.linalg.norm(drij, axis=-1, keepdims=True)\n",
    "    rt = r/sigma0\n",
    "    \n",
    "    ######## Modify code here ########\n",
    "    # Calculate constant values A and B based on rc, sigma0, epsilon0\n",
    "    B = 0\n",
    "    A = 0\n",
    "\n",
    "    ##################################\n",
    "\n",
    "    # Calculate LJ potential phi\n",
    "    phi = 4*epsilon0*(rt**(-12)-rt**(-6)) + A + B*r\n",
    "    phi[r > rc] = 0\n",
    "    f = -(4*epsilon0*( -12*rt**(-13) + 6*rt**(-7) )/sigma0 + B) * (drij/r)\n",
    "\n",
    "    return phi, f"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    " **(b) Plot the smoothly truncated LJ potential function**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# LJ parameters for Argon\n",
    "kB = 1.380662e-23/1.6021892e-19   # eV/K\n",
    "epsilon0 = 119.8*kB   # eV\n",
    "sigma0   = 3.405      # Angstrom\n",
    "rc = 2.37343077641*sigma0 # Angstrom\n",
    "\n",
    "rmax = 14\n",
    "r = np.linspace(3, rmax, 500)\n",
    "phi, f = ljval(r[:, np.newaxis], rc, sigma0, epsilon0)\n",
    "\n",
    "# plotting the LJ potential function\n",
    "plt.figure()\n",
    "plt.plot(r, phi, label=r'$\\phi$(eV)')\n",
    "plt.plot(r, f, label=r'$|\\vec{f}|$(eV/A)')\n",
    "plt.plot([0, rmax], [-epsilon0, -epsilon0], '--')\n",
    "plt.xticks(list(range(0, rmax+2, 3)) + [sigma0, rc], \n",
    "           list(range(0, rmax+2, 3)) + [r'$\\sigma_0$', r'$r_c$'], fontsize=12)\n",
    "plt.yticks([-epsilon0, 0], [r'$-\\epsilon_0$', 0], fontsize=12)\n",
    "plt.plot([0, rmax], [0, 0], '--k')\n",
    "plt.xlim([0, rmax])\n",
    "plt.ylim([-1.5*epsilon0, 3*epsilon0])\n",
    "plt.xlabel(r'$r(\\AA)$', fontsize=16)\n",
    "plt.legend(fontsize=16)\n",
    "plt.title('Modified Lennard-Jones Potential', fontsize=20)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**(c) Use MD++ to create atomistic structures for testing the `ljval` function**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "%%sh\n",
    "cd $MDPLUS_DIR\n",
    "make clean; make lj SYS=$MDPLUS_SYS build=R PY=yes\n",
    "\n",
    "$MDPLUS_EXE scripts/ME346B/ar_large.mdpp.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The energies of crystals of different sizes (4x4x4, 5x5x5, 6x6x6, 7x7x7) evaluated by MD++ are stored in the ``ar_eng.dat`` file in the following format, where N is the number of atoms, E is the potential energy.\n",
    "<pre>\n",
    "  N            E               E/N\n",
    "</pre>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%sh\n",
    "cd $MDPLUS_DIR\n",
    "cat runs/ar-large-example/ar_eng.dat"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The atomistic structures are stored in the files ``perf-4x4x4.cn``, ..., ``perf-7x7x7.cn`` in the `runs/ar-large-example` folder.  In the following, we will read these configurations into the notebook and use our own (Python) implementation of the LJ potential to evaluate the potential energy, and benchmark our results against MD++. The configuration can be loaded using `loadcn` function we provided in `mdutil.py` module, for example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# add the ME346B folder to path for loading utility functions\n",
    "import sys, time\n",
    "sys.path.insert(0, os.path.join(os.environ['MDPLUS_DIR'], 'scripts/ME346B'))\n",
    "\n",
    "from mdutil import loadcn\n",
    "\n",
    "runsdir = os.path.join(os.environ['MDPLUS_DIR'], 'runs/ar-large-example')\n",
    "cnfilename = os.path.join(runsdir, 'perf-6x6x6.cn')\n",
    "nparticles, s, h = loadcn(cnfilename)\n",
    "\n",
    "np.set_printoptions(suppress=True)\n",
    "print('Number of atoms: %d'%nparticles)\n",
    "print('scaled coordinates s.shape =', s.shape)\n",
    "print('simulation cell size:')\n",
    "print(h)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `loadcn` function returns `nparticles`, their scaled coordinates `s` and simulation box matrix `h`. \n",
    "\n",
    "In the following, we shall use the `ljval` function defined above to compute the potential energy of this Ar crystal structure, by evaluating potentials between all pairs of atoms.  This is not a very efficient way of computing the potential energy, because the computational cost is $O(N^2)$.\n",
    "\n",
    "Before we can test our Python implementation of the LJ potential, we need to take care of the periodic boundary condition (PBC).  To do so, we will implement a function that maps a collection of drij vectors to their corresponding minimum image vectors."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**<font size=+1>Exercise 2</font>**\n",
    "\n",
    "**Implement the minimum image convention** in periodic boundary conditions (PBC) by modifying the code block below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def pbc(drij, h, hinv=None):\n",
    "    '''calculate distance vector between i and j\n",
    "    \n",
    "        Considering periodic boundary conditions (PBC)\n",
    "    \n",
    "        Parameters\n",
    "        ----------\n",
    "        drij : float, dimension (npairs, 3)\n",
    "            distance vectors of atom pairs (Angstrom)\n",
    "        h : float, dimension (3, 3)\n",
    "            Periodic box size h = (c1|c2|c3)\n",
    "        hinv : optional, float, dimension (3, 3)\n",
    "            inverse matrix of h, if None, it will be calculated\n",
    "\n",
    "        Returns\n",
    "        -------\n",
    "        drij : float, dimension (npairs, 3)\n",
    "            modified distance vectors of atom pairs considering PBC (Angstrom)\n",
    "            \n",
    "    '''\n",
    "    if hinv is None:\n",
    "        hinv = np.linalg.inv(h)\n",
    "\n",
    "    ######## Add your code here ########\n",
    "    # modify drij to implement the minimum image convention\n",
    "    # e.g. if ri = (0,0,0.1) and rj = (0,0,Lz-0.1) then drij = -0.2 instead of Lz-0.2\n",
    "    \n",
    "    ####################################\n",
    "    return drij"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Define the `ljpot` function** based on `ljval` and `pbc` functions by executing the following block."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def ljpot(r, h, rc, sigma0, epsilon0):\n",
    "    '''LJ potential for configurations, considering periodic boundary condition\n",
    "    \n",
    "        Parameters\n",
    "        ----------\n",
    "        r : float, dimension (nparticles, 3)\n",
    "            *real* coordinate of atoms (Angstrom)\n",
    "        h : float, dimension (3, 3)\n",
    "            Periodic box size h = (c1|c2|c3)\n",
    "        rc : float\n",
    "            Lennard-Jones cut-off radius\n",
    "        sigma0, epsilon0 : float\n",
    "            parameters for Lennard-Jones potential\n",
    "\n",
    "        Returns\n",
    "        -------\n",
    "        Epot : float\n",
    "            total potential energy (eV)\n",
    "        F : float, dimension (nparticle, 3)\n",
    "            force vector on each atom (eV/A)\n",
    "            \n",
    "    '''\n",
    "    # initialization\n",
    "    nparticles = r.shape[0]\n",
    "    Epot = 0\n",
    "    F = np.zeros_like(r)\n",
    "    hinv = np.linalg.inv(h)\n",
    "    \n",
    "    # interaction between atoms\n",
    "    for i in range(nparticles):\n",
    "        ri = r[i, :]\n",
    "        for j in range(i+1, nparticles):\n",
    "            rj = r[j, :]\n",
    "            \n",
    "            # obtain the distance between atom i and atom j\n",
    "            drij = pbc(rj - ri, h, hinv)\n",
    "            \n",
    "            phi, fi = ljval(drij, rc, sigma0, epsilon0)\n",
    "            Epot += phi\n",
    "            F[i, :] = F[i, :] - fi\n",
    "            F[j, :] = F[j, :] + fi\n",
    "\n",
    "    return Epot, F"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Calculate the potential energy using ``ljpot``**\n",
    "\n",
    "Does your result agree with MD++ predictions (in Cell [8])?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# obtain real coordinates\n",
    "r = np.dot(h, s.T).T\n",
    "\n",
    "# time the potential evaluation\n",
    "tic = time.time()\n",
    "Epot, F = ljpot(r, h, rc, sigma0, epsilon0)\n",
    "toc = time.time()\n",
    "\n",
    "print('Lennard-Jones Potential Energy is %.12f eV'%Epot)\n",
    "print('O(N^2) algorithm time %.2f s'%(toc-tic))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Cell List and Neighbor(Verlet) List\n",
    "\n",
    "The bruteforce approach of computing potential energy by looping through all pairs is inefficient because the computational cost is $O(N^2)$. From the previous example, you would notice that the algorithm is time consuming. Because the interaction potential is short-ranged, it is not needed to consider pairs of atoms that are far away from each other. Neighbor list is a simple way to take advantage of this short-range restrictions, and reduce the interaction calculation to $O(N)$.\n",
    "\n",
    "In this section, you will implement the neighbor list in python, and see how it makes the energy evaluation more efficient."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Cell list\n",
    "\n",
    "When you are generating the neighbor list by going through all pairs of atoms, the complexity will still be $O(N^2)$. [Cell list](https://en.wikipedia.org/wiki/Cell_lists) is a method to find all pairs of atoms that has distance $r<r_c$ still within $O(N)$ time. As shown in the following figure (b), if we discretize the space into cells with size $r_c$, we only need to go through 9 cells in 2D (27 cells in 3D) in total to obtain all pairs with $r<r_c$. As the number of atoms grows, when the simulation box size $L>>r_c$, the cell list method reduces the computational time to $O(N)$.\n",
    "\n",
    "![Cell list](https://upload.wikimedia.org/wikipedia/en/6/62/CellLists.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Periodic Boundary Condition\n",
    "\n",
    "To represent bulk material, MD simulation usually uses periodic boundary condition (PBC) on all faces to avoid imposing arbitrary boundary conditions (free surface etc.). This is implemented within the cell list, by copying the cell on the other side as \"ghost\" cell out of the boundary, as shown in the following figure ([cite](https://en.wikipedia.org/wiki/Cell_lists)).\n",
    "\n",
    "![Ghost cell](https://upload.wikimedia.org/wikipedia/en/5/57/CellLists_Ghosts.png)\n",
    "\n",
    "### Neighbor(Verlet) List\n",
    "\n",
    "After the cell list is generated, it is straight-forward to implement the neighbor list by only going through the atoms within the neighboring 27 cells.\n",
    "\n",
    "\n",
    "**(a) define the celllist function**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def celllist(r, h, Nx, Ny, Nz):\n",
    "    '''Construct cell list in 3D\n",
    "    \n",
    "        This function takes the **real coordinates** of atoms `r` and the\n",
    "        simulation box size `h`. Grouping atoms into Nx x Ny x Nz cells\n",
    "        \n",
    "        Parameters\n",
    "        ----------\n",
    "        r : float, dimension (nparticles, 3)\n",
    "            *real* coordinate of atoms\n",
    "        h : float, dimension (3, 3)\n",
    "            Periodic box size h = (c1|c2|c3)\n",
    "        Nx : int\n",
    "            number of cells in x direction\n",
    "        Ny : int\n",
    "            number of cells in y direction\n",
    "        Nz : int\n",
    "            number of cells in z direction\n",
    "            \n",
    "        Returns\n",
    "        -------\n",
    "        cell : list, dimension (Nx, Ny, Nz)\n",
    "            each element cell[i][j][k] is also a list recording all \n",
    "            the indices of atoms within the cell[i][j][k].\n",
    "            (0 <= i < Nx, 0 <= j < Ny, 0 <= k < Nz)\n",
    "        cellid : int, dimension (nparticles, 3)\n",
    "            for atom i:\n",
    "            ix, iy, iz = (cellid[i, 0], cellid[i, 1], cellid[i, 2])\n",
    "            atom i belongs to cell[ix][iy][iz]\n",
    "\n",
    "    '''\n",
    "    # create empty cell list of size Nx x Ny x Nz\n",
    "    cell = [[[[] for k in range(Nz)] for j in range(Ny)] for i in range(Nx)]\n",
    "    \n",
    "    # create empty cell id list\n",
    "    nparticles = r.shape[0]\n",
    "    cellid = np.zeros((nparticles, 3), dtype=np.int)\n",
    "\n",
    "    # find reduced coordinates of all atoms\n",
    "    s = np.dot(np.linalg.inv(h), r.T).T\n",
    "    # fold reduced coordinates into [0, 1) as scaled coordinates\n",
    "    s = s - np.floor(s)\n",
    "\n",
    "    # create cell list and cell id list\n",
    "    for i in range(nparticles):\n",
    "        ix = np.floor(s[i, 0] * Nx).astype(np.int)\n",
    "        iy = np.floor(s[i, 1] * Ny).astype(np.int)\n",
    "        iz = np.floor(s[i, 2] * Nz).astype(np.int)\n",
    "        cell[ix][iy][iz].append(i)\n",
    "        cellid[i, :] = [ix, iy, iz]\n",
    "       \n",
    "    return cell, cellid"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**(b) define the verletlist function**\n",
    "\n",
    "This function generates a neighbor list (verlet list) after first creating a cell list.  The cut-off radius (rv) of the verlet list is supposed to be larger than the cut-off radius (rc) of the interatomic potential.  The difference, rv-rc, is called the skin thickness of the verlet list."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def verletlist(r, h, rv):\n",
    "    '''Construct Verlet List (neighbor list) in 3D\n",
    "    \n",
    "        Uses celllist to achieve O(N)\n",
    "    \n",
    "        Parameters\n",
    "        ----------\n",
    "        r : float, dimension (nparticles, 3)\n",
    "            *real* coordinate of atoms\n",
    "        h : float, dimension (3, 3)\n",
    "            Periodic box size h = (c1|c2|c3)\n",
    "        rv : float\n",
    "            Verlet cut-off radius\n",
    "            \n",
    "        Returns\n",
    "        -------\n",
    "        nn : int, dimension (nparticle, )\n",
    "            nn[i] is the number of neighbors for atom i\n",
    "        nindex : list, dimension (nparticle, nn)\n",
    "            nindex[i][j] is the index of j-th neighbor of atom i,\n",
    "            0 <= j < nn[i].\n",
    "            \n",
    "    '''\n",
    "    # first determine the size of the cell list\n",
    "    c1 = h[:, 0]; c2 = h[:, 1]; c3 = h[:, 2];\n",
    "    V = np.abs(np.linalg.det(h))\n",
    "    hx = np.abs( V / np.linalg.norm(np.cross(c2, c3)))\n",
    "    hy = np.abs( V / np.linalg.norm(np.cross(c3, c1)))\n",
    "    hz = np.abs( V / np.linalg.norm(np.cross(c1, c2)))\n",
    "    \n",
    "    # Determine the number of cells in each direction\n",
    "    Nx = np.floor(hx/rv).astype(np.int)\n",
    "    Ny = np.floor(hy/rv).astype(np.int)\n",
    "    Nz = np.floor(hz/rv).astype(np.int)\n",
    "    \n",
    "    if Nx < 2 or Ny < 2 or Nz < 2:\n",
    "        raise ValueError(\"Number of cells too small! Increase simulation box size.\")\n",
    "\n",
    "    # Inverse of the h matrix\n",
    "    hinv = np.linalg.inv(h);\n",
    "    cell, cellid = celllist(r, h, Nx, Ny, Nz)\n",
    "    \n",
    "    # Find the number of atoms\n",
    "    nparticles = r.shape[0]\n",
    "    \n",
    "    # initialize Verlet list\n",
    "    nn = np.zeros(nparticles)\n",
    "    nindex = [[] for i in range(nparticles)]\n",
    "    \n",
    "    for i in range(nparticles):\n",
    "        # position of atom i\n",
    "        ri = r[i, :]\n",
    "        \n",
    "        # find which cell (ix, iy, iz) that atom i belongs to\n",
    "        ix, iy, iz = (cellid[i, 0], cellid[i, 1], cellid[i, 2])\n",
    "        \n",
    "        # go through all neighboring cells\n",
    "        ixr = ix+1\n",
    "        iyr = iy+1\n",
    "        izr = iz+1\n",
    "        if Nx < 3:\n",
    "            ixr = ix\n",
    "        if Ny < 3:\n",
    "            iyr = iy\n",
    "        if Nz < 3:\n",
    "            izr = iz\n",
    "        \n",
    "        for nx in range(ix-1, ixr+1):\n",
    "            for ny in range(iy-1, iyr+1):\n",
    "                for nz in range(iz-1, izr+1):\n",
    "                    # apply periodic boundary condition on cell id nnx, nny, nnz\n",
    "                    nnx, nny, nnz = (nx%Nx, ny%Ny, nz%Nz)\n",
    "\n",
    "                    # extract atom id in this cell\n",
    "                    ind = cell[nnx][nny][nnz]\n",
    "                    nc = len(ind)\n",
    "                    \n",
    "                    # go through all the atoms in the neighboring cells\n",
    "                    for k in range(nc):\n",
    "                        j = ind[k]\n",
    "                        # update nn[i] and nindex[i]\n",
    "                        if i == j:\n",
    "                            continue\n",
    "                        else:\n",
    "                            rj = r[j, :]\n",
    "                            \n",
    "                            # obtain the distance between atom i and atom j\n",
    "                            drij = pbc(rj - ri, h, hinv)\n",
    "\n",
    "                            if np.linalg.norm(drij) < rv:\n",
    "                                nn[i] += 1\n",
    "                                nindex[i].append(j)\n",
    "                        \n",
    "    return nn, nindex"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**(c) define the ljpot_list function**\n",
    "\n",
    "This function evaluates the LJ potential using a neighbor list (verlet list)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def ljpot_list(r, h, rc, nn, nindex, sigma0, epsilon0):\n",
    "    '''LJ potential for configurations\n",
    "        \n",
    "        O(n) algorithm using Verlet list\n",
    "    \n",
    "        Parameters\n",
    "        ----------\n",
    "        r : float, dimension (nparticles, 3)\n",
    "            *real* coordinate of atoms (Angstrom)\n",
    "        h : float, dimension (3, 3)\n",
    "            Periodic box size h = (c1|c2|c3)\n",
    "        rc : float\n",
    "            Lennard-Jones cut-off radius\n",
    "        nn : int, dimension (nparticle, )\n",
    "            nn[i] is the number of neighbors for atom i,\n",
    "            output from verletlist().\n",
    "        nindex : list, dimension (nparticle, nn)\n",
    "            nindex[i][j] is the index of j-th neighbor of atom i,\n",
    "            0 <= j < nn[i], output from verletlist().\n",
    "        sigma0, epsilon0 : float\n",
    "            parameters for Lennard-Jones potential\n",
    "\n",
    "        Returns\n",
    "        -------\n",
    "        Epot : float\n",
    "            total potential energy (eV)\n",
    "        F : float, dimension (nparticle, 3)\n",
    "            force vector on each atom (eV/A)\n",
    "            \n",
    "    '''\n",
    "    # initialization\n",
    "    nparticles = r.shape[0]\n",
    "    Epot = 0\n",
    "    F = np.zeros_like(r)\n",
    "    hinv = np.linalg.inv(h)\n",
    "    \n",
    "    # interaction between atoms\n",
    "    for i in range(nparticles):\n",
    "        ri = r[i, :]\n",
    "        for j in nindex[i]:\n",
    "            if j > i:\n",
    "                rj = r[j, :]\n",
    "            \n",
    "                # calculate distance vector between i and j\n",
    "                drij = pbc(rj - ri, h, hinv)\n",
    "\n",
    "                phi, fi = ljval(drij, rc, sigma0, epsilon0)\n",
    "                Epot += phi\n",
    "                F[i, :] = F[i, :] - fi\n",
    "                F[j, :] = F[j, :] + fi\n",
    "\n",
    "    return Epot, F"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We then evaluate the LJ potential of the configuration using neighbor list:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cnfilename = os.path.join(os.environ['MDPLUS_DIR'], 'runs/ar-large-example/perf-6x6x6.cn')\n",
    "nparticles, s, h = loadcn(cnfilename)\n",
    "\n",
    "np.set_printoptions(suppress=True)\n",
    "print('Number of atoms: %d'%nparticles)\n",
    "print('scaled coordinates s.shape =', s.shape)\n",
    "print('simulation cell size:')\n",
    "print(h)\n",
    "\n",
    "r = np.dot(h, s.T).T\n",
    "rv = rc * 1.08\n",
    "tic = time.time()\n",
    "# construct neighbor list\n",
    "nn, nindex = verletlist(r, h, rv)\n",
    "# evaluating potential function using neighbor list\n",
    "Epot, F = ljpot_list(r, h, rc, nn, nindex, sigma0, epsilon0)\n",
    "toc = time.time()\n",
    "\n",
    "print('Lennard-Jones Potential Energy is %.12f eV'%Epot)\n",
    "print('O(N) algorithm time %.2f s'%(toc-tic))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can see that using the neighbor list, the algorithm is more efficient."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**<font size=+1>Exercise 3</font>**\n",
    "\n",
    "**Compare the computational time with and without the neighbor list**\n",
    "\n",
    "Write scripts in the following cell to read the atomistic configuration files for Ar crystals with 4x4x4, 5x5x5, 6x6x6, 7x7x7 sizes, and evaluating the potential energy using ``ljpot`` (without neighbor list) and ``ljpot_list`` (with neighbor list). Do they agree with each other?  \n",
    "\n",
    "Plot the computational time as a function of the number of atoms."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## Your code here\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.7"
  },
  "toc": {
   "base_numbering": "4",
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": true,
   "toc_position": {
    "height": "calc(100% - 180px)",
    "left": "10px",
    "top": "150px",
    "width": "288px"
   },
   "toc_section_display": true,
   "toc_window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
