# MD simulations of adatom on (001) surface of BCC Ta

import sys, os
mdpp_dir = os.environ['MDPLUS_DIR']
print('MD++ root directory:', mdpp_dir)
sys.path.insert(0, os.path.join(mdpp_dir, 'bin'))

import mdpp
import numpy as np

#********************************************
# Definition of functions
#********************************************

#--------------------------------------------
# Initialization
def initmd():
    #mdpp.cmd("setnolog")
    mdpp.cmd("setoverwrite")
    mdpp.cmd("dirname = runs/ta-001-int")
    mdpp.cmd("NNM = 200")

#--------------------------------------------
# Read the potential file
def readpot():
    mdpp.cmd('potfile = ' + os.path.join(mdpp_dir, 'potentials', 'ta_pot'))
    mdpp.cmd('readpot')

#--------------------------------------------
# Create Perfect Crystal Configuration
def create_crystal(n, a):
    # create perfect crystal of specified size n and lattice constant a
    # equilibrium lattice constant of Ta is 3.3058 Angstrom
    mdpp.cmd("crystalstructure = body-centered-cubic")
    mdpp.cmd("latticeconst = %f"%(a))
    mdpp.cmd("latticesize = [ 1 0 0 %d 0 1 0 %d 0 0 1 %d ]"%(n,n,n))
    mdpp.cmd("makecrystal finalcnfile = perf.cn writecn")
    mdpp.cmd("eval")

#--------------------------------------------
# Convert TAD .dat file to MD++ .cn file
def convert_dat_to_cn(infile, outfile):
    with open(infile) as fi:
        with open(outfile, 'w') as fo:
            line = fi.readline(); NP = int(line.split()[0])
            print("%d" % NP, file=fo)
            line = fi.readline(); 
            line = fi.readline(); box_size = [ float(a) for a in line.split() ]
            for i in range(NP):
                line = fi.readline(); ri = [float(a) for a in line.split()[0:3]]
                si = [ri[0]/box_size[0], ri[1]/box_size[1], ri[2]/box_size[2]]
                # shift scaled coordinates to center
                si[0] -= 0.5; si[1] -= 0.5; si[2] -= 0.25
                print("%20.12e %20.12e %20.12e" % (si[0], si[1], si[2]), file=fo)
            # write box matrix
            print("%20.12e %20.12e %20.12e" % (box_size[0], 0, 0), file=fo)
            print("%20.12e %20.12e %20.12e" % (0, box_size[1], 0), file=fo)
            print("%20.12e %20.12e %20.12e" % (0, 0, box_size[2]), file=fo)
            print("1 Ta", file=fo)
            print("%20.12e %20.12e" % (0, 0), file=fo)
            
    print("NP = %d" % NP)
    print("box_size = %s" % box_size) 

#--------------------------------------------
# Plot setting
def setup_window():
    mdpp.cmd('''
    atomradius = [1.0 0.78] bondradius = 0.3 bondlength = 0
    win_width=400 win_height=400
    #atomradius = 0.9 bondradius = 0.3 bondlength = 0 
    atomcolor = cyan highlightcolor = purple  bondcolor = red
    fixatomcolor = yellow backgroundcolor = gray70
    plot_atom_info = 1 # display reduced coordinates of atoms when clicked
    #plot_atom_info = 2 # display real coordinates of atoms
    #plot_atom_info = 3 # displayenergy of atoms
    plotfreq = 10
    #
    rotateangles = [ 0 0 0 1.5 ]
    ''')

def openwindow():
    setup_window()
    mdpp.cmd("openwin alloccolors rotate saverot eval plot")

#--------------------------------------------
# Conjugate-Gradient relaxation with box fixed
def relax_fixbox():
    mdpp.cmd(' conj_ftol = 1e-7 conj_itmax = 1000 conj_fevalmax = 10000 ')
    mdpp.cmd(' conj_fixbox = 1 #conj_monitor = 1 conj_summary = 1 ')
    mdpp.cmd(' relax ')

#--------------------------------------------
# MD simulation setting
def setup_md():
    mdpp.cmd('''
    equilsteps = 0  timestep = 0.001 # (ps)
    atommass = 180.94788 # (g/mol)
    DOUBLE_T = 1
    saveprop = 1 savepropfreq = 10 openpropfile #run
    savecn = 1 savecnfreq = 1000 openintercnfile
    #savecfg = 1 savecfgfreq = 1000
    plotfreq = 100 printfreq = 10
    vt2 = 1e28  #1e28 2e28 5e28
    wallmass = 2e4     # atommass * NP 
    boxdamp = 1e-3     #
    saveH # Use current H as reference (H0), needed for specifying stress
    fixboxvec  = [ 0 1 1 
                   1 0 1
                   1 1 0 ]
    output_fmt = "curstep EPOT KATOM Tinst HELM HELMP TSTRESSinMPa_xx TSTRESSinMPa_yy TSTRESSinMPa_zz H_11 H_22 H_33" 
    ''')

#*******************************************
# Main program starts here
#*******************************************
# status 0: load initial configuration and visualize
#        1: NVT simulations at precomputed thermal strain
#        2: skip
#        3: long NVT MD simulations to capture surface atom diffusion
#        4: compare relaxed structures from begining and end of long MD simulation
#
#--------------------------------------------
# Get command line argument
status = int(sys.argv[1]) if len(sys.argv) > 1 else 0
print('status = %d'%(status))

T = float(sys.argv[2]) if len(sys.argv) > 2 else 300
print('T = %f'%(T))

randseed = int(sys.argv[3]) if len(sys.argv) > 3 else 12345
print('randseed = %d'%(randseed))

if status == 0:
    # Load initial configuratin and visualize
    print("status == 0: NVE MD simulations")
    initmd()
    readpot()

    convert_dat_to_cn('../../scripts/ME346B/ta-001-int.start', 'ta-001-int.start.cn')
    mdpp.cmd('incnfile = ta-001-int.start.cn readcn')
    # reduce the empty space in z direction (was too much in original file)
    mdpp.cmd('input = [3 3 -0.5 ] changeH_keepR')
    mdpp.cmd('finalcnfile = ta-001-int.start.short.cn writecn')

    setup_window()
    # Comment out the following line if you don't want to open a window
    openwindow()

    sleep_seconds = 10

elif status == 1:
    # NVT MD simulations at precomputed thermal strain
    initmd()
    readpot()

    mdpp.cmd('incnfile = ta-001-int.start.short.cn readcn')

    # apply thermal expansion strain (alpha = 6.3e-6 K^-1, MSMSE 16, 085005, 2008)
    eps_T = 6.3e-6 * T
    Lx0 = mdpp.get('H_11')
    Ly0 = mdpp.get('H_22')
    mdpp.cmd("H_11 = %20.12e" % ( Lx0 * (1 + eps_T) ))
    mdpp.cmd("H_22 = %20.12e" % ( Ly0 * (1 + eps_T) ))
    setup_md()

    setup_window()
    # Comment out the following line if you don't want to open a window
    openwindow()

    mdpp.cmd('ensemble_type = "NVTC" integrator_type = "VVerlet"')
    mdpp.cmd("NHChainLen = 4 NHMass = [ 2e-3 2e-6 2e-6 2e-6 ]")
    mdpp.cmd("T_OBJ = %f DOUBLE_T = 1 randseed = %d srand48 initvelocity"%(T,randseed))
    mdpp.cmd("totalsteps = 100000 run")

    mdpp.cmd("finalcnfile = equil_fix_box.cn writevelocity = 1 writecn")

elif status == 2: 
    # NVT MD simulations that adjusts volume to reach zero stress
    print("skip status = %d"%(status))

elif status == 3:
    # NVT MD simulations: long simulations to capture surface atom diffusion
    initmd()
    readpot()

    mdpp.cmd('incnfile = equil_fix_box.cn readcn')

    setup_md()

    setup_window()
    # Comment out the following line if you don't want to open a window
    openwindow()

    mdpp.cmd('ensemble_type = "NVTC" integrator_type = "VVerlet"')
    mdpp.cmd("NHChainLen = 4 NHMass = [ 2e-3 2e-6 2e-6 2e-6 ]")
    # Note that we no longer need to use DOUBLE_T = 1 when randomizing the velocities for already equilibrated structures
    mdpp.cmd("T_OBJ = %f DOUBLE_T = 0 randseed = %d srand48 initvelocity"%(T,randseed))
    # plot_color_axis = 2 asks MD++ to compute central symmetry deviation (CSD) parameter at every step
    mdpp.cmd("plot_color_axis = 2  NCS = 8  writeall = 1")
    mdpp.cmd("totalsteps = 300000 run")

    mdpp.cmd("finalcnfile = final.cn writeall = 1 writecn")
    mdpp.cmd("finalcnfile = final.dump writeLAMMPS")

elif status == 4:
    # Compare relaxed structures from begining and end of long MD simulation
    # Your code here
    pass

elif status == 5:
    # Prepare configuration after atomic jump - displace atoms and then relax
    initmd()
    readpot()

    mdpp.cmd('incnfile = ta-001-int.start.short.cn readcn')

    setup_window()
    # Comment out the following line if you don't want to open a window
    openwindow()

    # Run a short MD and then relax to find a more stable configuratoin for State A
    setup_md()
    mdpp.cmd('ensemble_type = "NVTC" integrator_type = "VVerlet"')
    mdpp.cmd("NHChainLen = 4 NHMass = [ 2e-3 2e-6 2e-6 2e-6 ]")
    mdpp.cmd("T_OBJ = %f DOUBLE_T = 1 randseed = %d srand48 initvelocity"%(T,randseed))
    mdpp.cmd("totalsteps = 1000 run")
    relax_fixbox()
    mdpp.cmd('finalcnfile = ta-001-int.state_A.cn writecn')

    # displace atom on surface 
    dx = 0.0638 * 26.4
    dy = 0.0638 * 26.4
    dz = (0.1320 - 0.1610) * 50.0
    mdpp.cmd("input = [ %d %f %f %f %d ] moveatom " % (1, dx, dy,  dz, 0 ))
    mdpp.cmd("input = [ %d %f %f %f %d ] moveatom " % (1, dx, dy, -dz, 37))

    relax_fixbox()
    mdpp.cmd('finalcnfile = ta-001-int.state_B.cn writecn')

    sleep_seconds = 10

elif status == 6:
    # Calculate energy barrier between State A and State B
    initmd()
    readpot()

    mdpp.cmd('incnfile = ta-001-int.state_A.cn readcn setconfig1') # State A
    mdpp.cmd('incnfile = ta-001-int.state_B.cn readcn setconfig2') # State B
    mdpp.cmd('incnfile = ta-001-int.state_A.cn readcn ') # State A

    setup_window()
    # Comment out the following line if you don't want to open a window
    #openwindow()

    # Include all atoms in energy barrier calculation
    mdpp.cmd('fixallatoms constrain_fixedatoms freeallatoms ')

    mdpp.cmd('chainlength = 22 allocchain   totalsteps = 500')
    mdpp.cmd('timestep = 0.01 printfreq = 2')

    mdpp.cmd('initRchain ')

    mdpp.cmd('''
    stringspec = [ 0  #0: interpolate surrounding atoms, 1: relax surrounding atoms
                   1  #redistribution frequency
                 ]
    stringrelax''')

    mdpp.cmd('finalcnfile = string.chain.500 writeRchain')

elif status == 7:
    # Prepare State C from State A and then calculate energy barrier between State A and State C
    # Your code here
    pass

elif status == 8:
    # Compute stiffness (Hessian) matrix for State A
    initmd()
    readpot()

    mdpp.cmd('incnfile = ta-001-int.state_A.cn readcn ') # State A

    setup_window()
    # Comment out the following line if you don't want to open a window
    #openwindow()

    mdpp.cmd('eval ')

    # Compute Hessian matrix by finite difference, timestep specifies the displacement magnitude
    # input = 0 means including all atoms in the Hessian matrix calculation
    mdpp.cmd('timestep = 0.001 input = 0 calHessian ')

   # Results stored in file hessian.out

elif status == 9:
    # Compute stiffness (Hessian) matrix for Saddle State
    # (requires status == 6 be run before running this)
    initmd()
    readpot()

    # First load State A
    mdpp.cmd('incnfile = ta-001-int.state_A.cn readcn ') # State A

    # Load relaxed MEP
    mdpp.cmd('fixallatoms constrain_fixedatoms freeallatoms ')
    mdpp.cmd('chainlength = 22 allocchain   totalsteps = 500')
    mdpp.cmd('incnfile = string.chain.500 readRchain') 

    mdpp.cmd('input = 8   copyRchaintoCN ') # select Saddle State (chain_ID = 8)

    setup_window()
    # Comment out the following line if you don't want to open a window
    openwindow()

    # Compute Hessian matrix by finite difference, timestep specifies the displacement magnitude
    # input = 0 means including all atoms in the Hessian matrix calculation
    mdpp.cmd('timestep = 0.001 input = 0 calHessian ')

    # Results stored in file hessian.out
    sleep_seconds = 10

else:
    print("unknown status = %d"%(status))
  
#---------------------------------------------
# Sleep for a couple of seconds
import time
if not 'sleep_seconds' in locals():
    sleep_seconds = 2
print("Python is going to sleep for %d seconds."%sleep_seconds)
time.sleep(sleep_seconds)
