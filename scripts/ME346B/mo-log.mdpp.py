import sys, os
mdpp_dir = os.environ['MDPLUS_DIR']
print('MD++ root directory:', mdpp_dir)
sys.path.insert(0, os.path.join(mdpp_dir, 'bin'))

import mdpp

mdpp.cmd('''
# -*-shell-script -*-
# make perfect crystal of Molybdenum
#
#setnolog
setoverwrite
dirname = runs/mo-example
''')

# Read the potential file
mdpp.cmd('potfile = ' + os.path.join(mdpp_dir, 'potentials', 'mo_pot'))
mdpp.cmd('readpot')

mdpp.cmd('''
# Create Perfect Crystal
crystalstructure = body-centered-cubic
latticeconst = 3.1472		# in Angstrom for Mo
latticesize = [ 1 0 0 3
		0 1 0 3
		0 0 1 3 ]
makecrystal
finalcnfile = "perf.cn" writecn
#
#-----------------------------------------
# Plot Configuration 
#
atomradius = 1.0 bondradius = 0.3 bondlength = 2.8285 #for Si
atomcolor = orange highlightcolor = purple  
bondcolor = red backgroundcolor = gray70
plotfreq = 10  rotateangles = [ 0 0 0 1.25 ] 
openwin  alloccolors rotate saverot eval plot
''')

import time
sleep_seconds = 10
print("Python is going to sleep for %d seconds."%sleep_seconds)
time.sleep(sleep_seconds)
