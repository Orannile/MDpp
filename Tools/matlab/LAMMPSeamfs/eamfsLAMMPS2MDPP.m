LAMMPS_potfile = 'CuZr.eam.fs';
MDPP_potfile = 'eamdata.CuZr.ZJU.eam.fs';

[griddata, elemdata, Frhodata, rhordata, phirdata] = readLAMMPSeamfs(LAMMPS_potfile);
writeMDPPeamfs(MDPP_potfile,griddata, elemdata, rhordata, phirdata, Frhodata);
