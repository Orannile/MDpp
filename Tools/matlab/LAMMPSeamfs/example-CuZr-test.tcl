# -*-shell-script-*-
# MD code of EAM Cu-Zr
# package require math

#*******************************************
# Definition of procedures
#*******************************************
proc initmd { T frac} { MD++ {
# setnolog
setoverwrite }
MD++ dirname = runs/ZrCu-test-$T-$frac
}

proc readeam_Zr_Cu_alloy {} { MD++ {
# Make sure the sequence of the atom spcies is correct and the eamgrid exactly match the pot file
    potfile = ../../potentials/EAMDATA/eamdata.CuZr.LAMMPS
    eamgrid = 1001  eamfiletype = 2 # binary alloy type potential
    readeam
    atommass = [ 91.224 63.546 ] # Zr Cu
    element0 = "Zr" element1 = "Cu"
    NNM = 200
}
puts "The EAM Zr-Cu alloy potential is sucessfully loaded"
}

proc readeam_Cu_Zr_fs {} { MD++ {
# Make sure the sequence of the atom spcies is correct and the eamgrid exactly match the pot file
    # eam potential with rho_ab(r)
    potfile = ../../Tools/matlab/LAMMPSeamfs/eamdata.CuZr.ZJU.eam.fs
    eamgrid = 10000 eamfiletype = 4 # binary fs type potential
    readeam
    atommass = [ 63.546 91.224 ] # Cu Zr
    element0 = "Cu" element1 = "Zr"
    NNM = 600
}
puts "The EAM Cu-Zr fs potential is sucessfully loaded"
}

proc makecrystal { nx ny nz } {
#Create Perfect FCC Lattice Configuration
#
MD++ crystalstructure = face-centered-cubic
#MD++ latticeconst = 3.6030 #(A) for Cu
MD++ latticeconst = 4.56 #(A) for Zr (assuming FCC)
MD++ nspecies = 2
#MD++ element0 = "Zr" element1 = "Cu"
#MD++ element0 = "Cu" element1 = "Zr"
MD++ latticesize  = \[ 1 0 0 $nx 0 1 0 $ny 0 0 1 $nz \]
MD++ makecrystal
}

proc makealloy { frac } {

set Ntot [MD++_Get NP]
set Ntot_1 [expr $Ntot - 1]
set N_Cu [ expr $Ntot*$frac/100.0]
set nums {}

while {[llength $nums] < $N_Cu} {
       #set n [::math::random 0 $Ntot_1]
       set n [ expr floor( rand() * $Ntot_1 ) ]
       if {$n ni $nums} {lappend nums $n}
       puts $n
   }
   set nums [linsert $nums 0 {}]
   for {set i 1} {$i <= $N_Cu} {incr i} {
       set fnod($i) [lindex $nums $i]
   }

for { set x 1 } { $x <= $N_Cu } { incr x 1 } {
   MD++ input=\[ 1 $fnod($x) \]
   MD++ fixatoms_by_ID
}

set element0 [MD++_Get "element0"]
puts "element0 = $element0"

if {[string equal $element0 "Cu"]} {
   MD++ reversefixedatoms
}
MD++  input = 1 setfixedatomsspecies freeallatoms
}

#-------------------------------------------------------------
proc setup_window { } {
#Plot Settings
#
set element0 [MD++_Get "element0"]
if {[string equal $element0 "Cu"]} {
  MD++ atomradius = \[ 0.67 0.85 \] #bondradius = 0.3 bondlength = 0
  MD++ atomcolor0 = cyan atomcolor1 = orange
} else {
  MD++ atomradius = \[ 0.85 0.67 \] #bondradius = 0.3 bondlength = 0
  MD++ atomcolor0 = orange atomcolor1 = cyan
}
MD++ highlightcolor = purple  bondcolor = red backgroundcolor = gray70 fixatomcolor = yellow
MD++ plot_atom_info = 1
} 

#-------------------------------------------------------------
proc openwindow { } {
#Configure and open a plot
#
setup_window
MD++ openwin  alloccolors rotate saverot eval plot
}

#-------------------------------------------------------------
proc relax_fixbox { } { MD++ {
#Conjugate-Gradient relaxation
#
conj_ftol = 1e-9 conj_itmax = 10000 conj_fevalmax = 100000
conj_fixbox = 1 #conj_monitor = 1 conj_summary = 1
relax
} }

#-------------------------------------------------------------
proc relax_freebox { } { MD++ {
#Conjugate-Gradient relaxation
#
conj_ftol = 1e-7 conj_itmax = 1000 conj_fevalmax = 1000
conj_fixbox = 0 #conj_monitor = 1 conj_summary = 1
conj_fixboxvec = [ 0 1 1  
                   1 0 1   
                   1 1 0 ]
relax
} }

#-------------------------------------------------------------
proc setup_md { } { MD++ {
#MD settings (without running MD)
#
equilsteps = 0  totalsteps = 1000 timestep = 0.0001 # (ps)
atommass = [91.224 63.546 ] # (g/mol)
atomTcpl = 200.0 boxTcpl = 20.0
DOUBLE_T = 0
srand48bytime
initvelocity totalsteps = 10000 saveprop = 0
saveprop = 1 savepropfreq = 10 openpropfile
savecn = 1  savecnfreq = 100
writeall = 1
savecfg = 1 savecfgfreq = 100
ensemble_type = "NVT" integrator_type = "VVerlet"
implementation_type = 0 vt2=1e28
totalsteps = 20000
output_fmt = "curstep EPOT KATOM Tinst HELM HELMP TSTRESS_xx TSTRESS_yy TSTRESS_zz"
#output_fmt = "curstep EPOT KATOM Tinst HELM HELMP TSTRESS_xx TSTRESS_yy TSTRESS_zz"
plotfreq = 50 #autowritegiffreq = 10
} }


#*******************************************
# Main program starts here
#*******************************************

# read in status from command line argument
if { $argc == 0 } {
 set status 0
} elseif { $argc > 0 } {
 set status [lindex $argv 0]
}
puts "status = $status"

if { $argc <=1 } {
  set T_OBJ 300
} elseif { $argc > 1 } {
 set T_OBJ [lindex $argv 1]
 set n_start [lindex $argv 1]
}
puts "T_OBJ = $T_OBJ"

if { $argc <=2 } {
  set frac 0
} elseif { $argc > 2 } {
 set frac [lindex $argv 2]
 set num_files [lindex $argv 2]
}
puts "Cufrac = $frac"

initmd $T_OBJ $frac

#readeam_Zr_Cu_alloy
readeam_Cu_Zr_fs

if { $status == 0 } {
  #Create crystal and relax
  makecrystal  5 5 5
  expr srand( 1234 )
  makealloy $frac
  # openwindow

  MD++ eval
  MD++ nspecies = 2
  MD++ finalcnfile = init_ZrCu.cn writeall = 1 writecn
  MD++ finalcnfile = init_ZrCu.lammps writeLAMMPS
  MD++ finalcnfile = init_ZrCu.cfg writeatomeyecfg
  MD++ input = 0 finalcnfile = "force_init.txt" writeFORCE

  MD++ input = \[ 1 0.1  0.2  0.3   0 \] moveatom
  MD++ input = \[ 1 0.1 -0.1 -0.15  1 \] moveatom
  relax_fixbox
  #relax_freebox

  MD++ nspecies = 2
  MD++ finalcnfile = relaxed_ZrCu.cn writeall = 1 writecn
  MD++ finalcnfile = relaxed_ZrCu.lammps writeLAMMPS
  MD++ finalcnfile = relaxed_ZrCu.cfg writeatomeyecfg
  MD++ eval
  MD++ input = 0 finalcnfile = "force_relaxed.txt" writeFORCE
  MD++ sleep quit

} elseif { $status == 1} {
  #Read in relaxed, NVT

  MD++ incnfile = "relaxed_ZrCu.cn" readcn
  openwindow
  setup_md
  MD++ T_OBJ = $T_OBJ
  MD++ totalsteps = 2000
  MD++ run
  MD++ nspecies = 2
  MD++ finalcnfile = final_ZrCu.cn writeall = 1 writecn
  MD++ finalcnfile = final_ZrCu.cfg writeatomeyecfg
  MD++ finalcnfile = final_ZrCu.lammps writeLAMMPS
  MD++ sleep quit

} elseif { $status == 2} {
  #Read in LAMMPS *DUMP* file and evalulate energy and force

  file copy -force ../../Tools/matlab/LAMMPSeamfs/init_config.dump .
  MD++ incnfile = init_config.dump
  MD++ input=\[ 0 0 \]
  MD++ readLAMMPS
  MD++ eval
  MD++ finalcnfile = "force_lammps.txt" writeFORCE
  MD++ nspecies = 2
  MD++ finalcnfile = lammps_ZrCu.cn writeall = 1 writecn
  MD++ finalcnfile = lammps_ZrCu.cfg writeatomeyecfg
  MD++ finalcnfile = lammps_ZrCu.lammps writeLAMMPS
  MD++ quit

} else {
  puts "unknown argument. status = $status"
  MD++ quit
}

MD++ quit
