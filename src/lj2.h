/*
  lj2.h
  by Wei Cai  caiwei@mit.edu
  Last Modified : Fri Feb 16 09:59:52 2007

  FUNCTION  :  Two component Lennard-Jones potential, depending on group ID
*/

#ifndef _LJ2_H
#define _LJ2_H

#include "mdparallel.h"

//All physical constants starts with P_
#define P_KB 1.380662e-23  // (kg*m^2/s^2/K) Boltzmann constant
#define P_E 1.6021892e-19  // (C) electron charge

//#define LJ_ENERGY (119.8*P_KB/P_E) // argon energy (sigma)
//#define LJ_LENGTH 3.405            // argon length (epsilon)
//#define LJ_RC     (2.37343077641*LJ_LENGTH) //4 neighbor

class LJ2Frame : public MDPARALLELFrame /* Si with Stillinger-Weber potential */
{
    /* Lennard-Jones parameters */
    double _ALJ_00,_BLJ_00, ALJ_00,BLJ_00, Uc_00,DUDRc_00;
    double _ALJ_11,_BLJ_11, ALJ_11,BLJ_11, Uc_11,DUDRc_11;
    double _ALJ_01,_BLJ_01, ALJ_01,BLJ_01, Uc_01,DUDRc_01;
    double LJ_ENERGY, LJ_LENGTH, LJ_RC;

    int LJ_2D;

    /* heat flux variables */
    Vector3 JE, JE_INT;
    
public:
    LJ2Frame(): _ALJ_00(0), _BLJ_00(0), ALJ_00(0), BLJ_00(0), Uc_00(0), DUDRc_00(0),
                _ALJ_11(0), _BLJ_11(0), ALJ_11(0), BLJ_11(0), Uc_11(0), DUDRc_11(0),
                _ALJ_01(0), _BLJ_01(0), ALJ_01(0), BLJ_01(0), Uc_01(0), DUDRc_01(0),
                LJ_ENERGY(0), LJ_LENGTH(0), LJ_RC(0),
                LJ_2D(0) {};

    void lennard_jones_2();
    virtual void potential();
    virtual void initvars();

    virtual void initparser();
    virtual int exec(const char *name);
    virtual void calcprop();
    
    void initLJ();
    void makeliquid();
    void randomposition_rc(double rc);
    void clear_vz();
};



#endif // _LJ2_H

